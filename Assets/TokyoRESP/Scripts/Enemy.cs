﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

	[SerializeField] private NavMeshAgent agent;
	[SerializeField] private float distanceFromTarget;
	[SerializeField] private GameObject explosionPrefab;
	[SerializeField] private float enemySpeed = 1;
	private Vector3 playerBasePosition;
	private bool attack;
	
	void Start ()
	{
		playerBasePosition = GameObject.Find("PlayerBase").transform.position;
		agent.SetDestination(playerBasePosition);
		agent.speed = enemySpeed;
	}
	
	
	void Update ()
	{
		distanceFromTarget = Vector3.Distance(transform.position, playerBasePosition);
		if (distanceFromTarget < 1 && !attack)
		{
			attack = true;
			StartCoroutine(AttackRoutine());
		}
	}

	IEnumerator AttackRoutine()
	{
		GameObject playerHQ = GameObject.Find("HeadQuarters"); 
		yield return new WaitForSeconds(1);
		TowerGameManager.Instance.EnemiesOnScene--;
		Instantiate(explosionPrefab, playerHQ.transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
