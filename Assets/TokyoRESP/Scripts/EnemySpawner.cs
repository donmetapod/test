﻿using System;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Game;
using TowerDefense.Level;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

	public enum enemytypes
	{
		buggy,
		helicopter,
		hover
	};

	[Serializable]
	public struct Waves
	{
		public enemytypes[] wave;
	}

	public Waves[] waves;

	[SerializeField] private GameObject buggy;
	[SerializeField] private GameObject hover;
	[SerializeField] private GameObject helicopter;
	[SerializeField] private int currentWave = 0;
	[SerializeField] private int currentEnemy = 0;
	[SerializeField] private int timeBetweenWaves = 5;
	
	void Update () {
		
	}

	public IEnumerator SendWave()
	{
		UIManager.Instance.UpdateWaveNumber(currentWave+1, waves.Length);
		
		while (currentEnemy < waves[currentWave].wave.Length)
		{
			yield return new WaitForSeconds(3);
			switch (waves[currentWave].wave[currentEnemy])
			{
				case enemytypes.buggy:
					Instantiate(buggy, transform.position, Quaternion.identity);
					break;
				case enemytypes.hover:
					Instantiate(hover, transform.position, Quaternion.identity);
					break;
				case enemytypes.helicopter:
					Instantiate(helicopter, transform.position, Quaternion.identity);
					break;
			}

			TowerGameManager.Instance.EnemiesOnScene++;
			currentEnemy++;
			StartCoroutine(UIManager.Instance.UpdateWaveProgress(currentEnemy, waves[currentWave].wave.Length));
		}

		yield return new WaitForSeconds(timeBetweenWaves);

		currentWave++;
		if (currentWave < waves.Length)
		{
			currentEnemy = 0;
			StartCoroutine(SendWave());
		}
		else
		{
			Debug.Log("waves ended");
			TowerGameManager.Instance.spawningHasFinished = true;
		}
	}
}
