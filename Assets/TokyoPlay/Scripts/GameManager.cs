﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace tec 
{
	public class GameManager : MonoBehaviour
	{

		private static GameManager tpGameManager;
		public static GameManager Instance
		{
			get
			{
				return tpGameManager;
			
			}
			set
			{
				tpGameManager = value; 
			}
		}

	
		public bool wavesEnded;
		private int enemiesOnScene = 0;
		public int EnemiesOnScene
		{
			get
			{
				return enemiesOnScene;
			
			}
			set
			{
				enemiesOnScene = value;
				if (enemiesOnScene <= 0 && wavesEnded && hqHealth > 0)
				{
					UIManager.Instance.gameOverPanel.SetActive(true);
					UIManager.Instance.gameOverText.text = "WELL DONE!!!";
					Invoke("LoadMainMenu", 3);
				}
			}
		}
	
		private int hqHealth = 10;
		public int HqHealth
		{
			get { return hqHealth; }
			set
			{
				hqHealth = value;
				UIManager.Instance.hqHealth.text = hqHealth.ToString();
				if (hqHealth <= 0)
				{
					UIManager.Instance.gameOverPanel.SetActive(true);
					UIManager.Instance.gameOverText.text = "YOU LOSE";
					Invoke("LoadMainMenu", 3);
				}
			}
		}
	
		public List<Transform> enemyList = new List<Transform>();
		public int playerEnergy = 10;
	
		private void Awake()
		{
			if (Instance == null)
			{
				Instance = this;
			}
			else
			{
				Destroy(gameObject);
			}
		}

		public IEnumerator IncreaseEnergy()
		{
			while (true)
			{
				yield return new WaitForSeconds(1);
				playerEnergy++;
				UIManager.Instance.UpdateEnergyUI();
			} 
		}

		void LoadMainMenu()
		{
			SceneManager.LoadScene("TPMainMenu");
		}
	}
	

}
