﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tec
{
	public class PLayer : MonoBehaviour {

		[SerializeField] private GameObject machineGunPrefab;
		[SerializeField] private GameObject missilePrefab;
		[SerializeField] private GameObject sniperPrefab;
		[SerializeField] private GameObject holdingWeapon;
	
		public void CreateWeapon(string weaponType)
		{
			if(holdingWeapon != null)
				return;

			if (GameManager.Instance.playerEnergy > 4)
			{
				switch (weaponType)
				{
					case "gun":
						holdingWeapon = Instantiate(machineGunPrefab);
						GameManager.Instance.playerEnergy -= 4;
						break;
					case "missile":
						holdingWeapon = Instantiate(missilePrefab);
						GameManager.Instance.playerEnergy -= 4;
						break;
					case "sniper":
						holdingWeapon = Instantiate(sniperPrefab);
						GameManager.Instance.playerEnergy -= 4;
						break;
				}
				UIManager.Instance.UpdateEnergyUI();
			}
		}

		private void Update()
		{
			if (holdingWeapon != null)
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hitInfo;
				if (Physics.Raycast(ray, out hitInfo))
				{
					holdingWeapon.transform.position = hitInfo.point;
					if (Input.GetMouseButtonDown(0))
					{
						if (hitInfo.transform.CompareTag("Placement"))
						{
							holdingWeapon = null;
							Destroy(hitInfo.collider);
						}
					}
				}
			}
		}
	}
	

}
