﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tec
{
	public class EnemySpawner : MonoBehaviour
	{

		public enum EnemyTypes
		{
			buggy,
			helicopter,
			hover
		};

		[Serializable]
		public struct Waves
		{
			public EnemyTypes[] wave;	
		}

		public Waves[] waves;

		[SerializeField] private GameObject buggyPrefab;
		[SerializeField] private GameObject helicopterPrefab;
		[SerializeField] private GameObject hoverPrefab;
		[SerializeField] private int currentWave = 0;
		[SerializeField] private int currenEnemy = 0;
		[SerializeField] private int timeBetweenWaves = 5;

		public IEnumerator SendWave()
		{
			UIManager.Instance.UpdateWaveNumber(currentWave+1, waves.Length);
			while (currenEnemy < waves[currentWave].wave.Length)
			{
			
				switch (waves[currentWave].wave[currenEnemy])
				{
					case EnemyTypes.buggy:
						Instantiate(buggyPrefab, transform.position, Quaternion.identity);
						break;
					case EnemyTypes.helicopter:
						Instantiate(helicopterPrefab, transform.position, Quaternion.identity);
						break;
					case EnemyTypes.hover:
						Instantiate(hoverPrefab, transform.position, Quaternion.identity);
						break;
				}

				yield return new WaitForSeconds(3);
				currenEnemy++;
				StartCoroutine(UIManager.Instance.UpdateWaveProgress(currenEnemy, waves[currentWave].wave.Length));
			}
			currentWave++;
			if (currentWave < waves.Length)
			{
				currenEnemy = 0;
				yield return new WaitForSeconds(timeBetweenWaves);
				StartCoroutine(SendWave());
			}
			else
			{
				GameManager.Instance.wavesEnded = true;
			}

		}

	}
	

}
