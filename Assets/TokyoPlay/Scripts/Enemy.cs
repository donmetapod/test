﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace tec
{
	public class Enemy : MonoBehaviour
	{

		[SerializeField] private float distanceFRomTarget;
		[SerializeField] private GameObject explosionPrefab;
		[SerializeField] private int health = 10;
		[SerializeField] private GameObject bulletHitPrefab;
	
		NavMeshAgent agent;
		private GameObject target;
		private bool reachedTarget;
	
		void Start ()
		{
			agent = GetComponent<NavMeshAgent>();
			target = GameObject.Find("PlayerBaseTarget");
			agent.SetDestination(target.transform.position);
			GameManager.Instance.EnemiesOnScene++;
			GameManager.Instance.enemyList.Add(transform);
		}
	
	
		void Update ()
		{
			distanceFRomTarget = Vector3.Distance(transform.position, target.transform.position);
			if (distanceFRomTarget < 0.1f && !reachedTarget)
			{
				reachedTarget = true;
				StartCoroutine(ReachTarget());
			}
		}

		IEnumerator ReachTarget()
		{
			GameObject hq = GameObject.Find("HeadQuarters");
			yield return new WaitForSeconds(1);
			Instantiate(explosionPrefab, hq.transform.position, Quaternion.identity);
			TakeDamage(health);
			GameManager.Instance.HqHealth--;
			GameManager.Instance.enemyList.Remove(transform);
			GameManager.Instance.EnemiesOnScene--;
			Destroy(gameObject);
		}

		[SerializeField] private bool dead;
		public void TakeDamage(int amount)
		{
			health -= amount;
			Instantiate(bulletHitPrefab, transform.position +  new Vector3(0,0.5f,0), Quaternion.identity);
			if (health <= 0 && !dead)
			{
				dead = true;
				Instantiate(explosionPrefab, transform.position, Quaternion.identity);
				GameManager.Instance.enemyList.Remove(transform);
				GameManager.Instance.EnemiesOnScene--;
				Destroy(gameObject);
			}
		}
	}
	

}
