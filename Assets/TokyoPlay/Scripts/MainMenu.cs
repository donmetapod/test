﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace tec{
    public class MainMenu : MonoBehaviour
    {
    
        [SerializeField]private GameObject mainMenuPanel;
        [SerializeField]private GameObject levelSelectPanel;
        [SerializeField]private GameObject authenticationPanel;
        [SerializeField] private InputField usernameField;
        [SerializeField] private InputField passwordField;
        [SerializeField] private Animator cameraAnim;
            
        public void QuitGame()
        {
            Debug.Log("Quit game");
            Application.Quit();
        }
    
        public void ShowLevelSelect()
        {
            mainMenuPanel.SetActive(false);
            levelSelectPanel.SetActive(true);
            cameraAnim.SetTrigger("ToLevelSelect");
        }
    
        public void ShowMainMenu()
        {
            mainMenuPanel.SetActive(true);
            levelSelectPanel.SetActive(false);
            cameraAnim.SetTrigger("ToMainMenu");
        }
    
        public void RegisterPlayer()
        {
            PlayerPrefs.SetString("username", usernameField.text);
            PlayerPrefs.SetString("password", passwordField.text);
            PlayerPrefs.Save();
            usernameField.text = null;
            passwordField.text = null;
        }
    
        public void PlayerLogin()
        {
            if (usernameField.text == PlayerPrefs.GetString("username") && passwordField.text == PlayerPrefs.GetString("password"))
            {
                Debug.Log("show main menu");
                authenticationPanel.SetActive(false);
                mainMenuPanel.SetActive(true);
            }
            else
            {
                Debug.Log("user not found");
            }
        }
    
        public void LoadLevel(string level)
        {
            SceneManager.LoadScene("Level"+level);
        }
    }

}
