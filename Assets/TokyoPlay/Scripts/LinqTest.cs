﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LinqTest : MonoBehaviour {

	[SerializeField] List<Transform> cubes = new List<Transform>();
	
	
	void Start ()
	{
		/*float smallestDistance = Mathf.Infinity;
		Transform nearest = null;

		foreach (var cube in cubes)
		{
			var distance = Vector3.Distance(transform.position, cube.transform.position);
			if (distance < smallestDistance)
			{
				smallestDistance = distance;
				nearest = cube;
			}
		}
		
		Debug.Log(nearest.name);*/
		//StartCoroutine(DoLinq());

	}

	IEnumerator DoLinq()
	{
		var nearest = cubes.OrderByDescending(t => Vector3.Distance(transform.position, t.transform.position)).ToList().FirstOrDefault();
		
		
		yield return new WaitForSeconds(4);
		
		Debug.Log(nearest.name);
	}


}
