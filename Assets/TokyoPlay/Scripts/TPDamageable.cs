﻿using System.Collections;
using System.Collections.Generic;
using ActionGameFramework.Audio;
using UnityEngine;

 public abstract class TPDamageable : MonoBehaviour
 {

	 private int health = 10;
	 
	 public void TakeDamage(int amount)
	 {
		 health -= amount;
		 Debug.Log("Making damage");
	 }
 }
